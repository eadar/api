module.exports = {
    root: true,
    env: {
        browser: false,
        node: true
    },
    extends: [
        'eslint:recommended',
        'plugin:node/recommended'
    ],
    rules: {
        'no-console': 'off',
        'node/no-unpublished-require': 'off'
    }
};
