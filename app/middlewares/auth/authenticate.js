const config = require( '../../config' );
const jwt = require( 'jsonwebtoken' );

module.exports = ctx => {

    if ( ctx.request.body.password === 'password' ) {

        const test = jwt.sign( {
                username: 'WanFoxOne',
                role: 'admin'
            },
            config.jwt.secret, {
                algorithm: 'HS512',
                expiresIn: 30
            } );
        const test2 = jwt.verify( test, config.jwt.secret );

        ctx.status = 200;
        ctx.body = {
            token: test,
            decode: jwt.decode( test, { complete: true } ),
            verify: test2,
            message: 'Successfully logged in!'
        };

    }
    else {

        ctx.status = ctx.status = 401;

        ctx.body = {
            message: 'Authentication failed'
        };
    }
    return ctx;
};
