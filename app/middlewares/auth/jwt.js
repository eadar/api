const config = require( '../../config' );
const koaJwt = require( 'koa-jwt' );

module.exports = koaJwt( {
    secret: config.jwt.secret
} );
