module.exports = {
    async json( ctx, next ) {
        ctx.set( 'Content-Type', 'application/json' );
        await next();
    }
};
