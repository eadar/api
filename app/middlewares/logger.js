module.exports = {
    async requests( ctx, next ) {

        const start = Date.now();

        await next();

        const delta = Math.ceil(Date.now() - start);

        ctx.set('X-Response-Time', `${delta}ms`);
        console.log( `${ctx.method} ${ctx.url} - ${delta}ms` );
    }
};
