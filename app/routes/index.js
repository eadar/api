const fs = require( 'fs' );
const path = require( 'path' );

const router = require( 'koa-router' )( { prefix: '' } );

/**
 * Generate routes with tree structure
 * @param dir
 */
const generation = async dir => {

    fs.readdir( dir, ( err, items ) => {
        if ( err ) throw err;

        for ( const item of items ) {
            const _item = path.resolve( dir, item );

            fs.stat( _item, function ( err, stat ) {
                if ( err ) throw err;

                if ( stat && stat.isDirectory() ) {
                    generation( _item );
                }
                else {

                    // Exclude master
                    if ( dir === __dirname && item === 'index.js' ) return;

                    // Generate route
                    const require_dir = _item.replace( __dirname, '' ).replace( item, '' ).replace( /\\/g, '/' );
                    const _require_path = `.${require_dir}${item}`;

                    require( _require_path )( require_dir.replace( /\/$/g, '' )).then( item => {
                        router.use( item.routes() );
                    } );
                }
            } );
        }
    } );
};

// Launch routes generation
generation( __dirname );

module.exports = router;
