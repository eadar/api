module.exports = async prefix => {

    const router = require( 'koa-router' )( { prefix } );

    router.get( '/', async ( ctx ) => {

        ctx.body = {
            message: "ROOT"
        };

    } );

    return router;
};
