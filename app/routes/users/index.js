const jwt = require( '../../middlewares/auth/jwt' );

module.exports = async prefix => {

    const router = require( 'koa-router' )( { prefix } );

    router.get( '/', jwt, async ( ctx ) => {

        let research = await ctx.db.query(
            'MATCH (n:Users) RETURN n'
        ).catch( console.error );

        ctx.body = {
            users: {
                registered: research.records.length
            }
        };

    } );

    return router;
};
