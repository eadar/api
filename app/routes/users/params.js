const jwt = require( '../../middlewares/auth/jwt' );

module.exports = async prefix => {

    const router = require( 'koa-router' )( { prefix } );

    router.get( '/:username', jwt, async ( ctx ) => {

        const username = ctx.params.username.trim();

        let render = result => {
            const user = result.records[ 0 ].get( 0 ).properties;
            return ctx.body = {
                users: {
                    username: user.username,
                    status: 'active'
                }
            };
        };

        // Sensitive research
        let sensitive_research = await ctx.db.query(
            'MATCH (n:Users { username: $username }) RETURN n',
            { username }
        ).catch( console.error );

        if ( sensitive_research.records.length ) {
            render( sensitive_research );
            return;
        }

        let insensitive_research = await ctx.db.query(
            `MATCH (n:Users) WHERE n.username =~ '(?i)${username}' RETURN n`
        ).catch( console.error );

        if ( insensitive_research.records.length ) {
            render( insensitive_research );
            return;
        }

        ctx.response.status = 400;

    } );

    return router;
};
