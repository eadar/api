const Neo4j = require( 'neo4j-driver' ).v1;

// Database drivers
const drivers = config => ({
    neo4j_1: Neo4j.driver( config.neo4j.host, Neo4j.auth.basic( config.neo4j.user, config.neo4j.pass ) )
});

// Database session
module.exports = config => ({

    /**
     * Running a Query
     *
     * @param cypher
     * @param params
     * @returns {Promise<StatementResult>}
     */
    async query( cypher, params ) {

        const driver = drivers( config ).neo4j_1;
        const session = driver.session();

        try {
            return await session.run( cypher, params ).then( result => {

                session.close();
                driver.close();

                return result;
            } );

        } catch ( error ) {
            throw new Error( error );
        }

    },

    /**
     * Convert id to Neo4j float type
     * @param id
     * @returns {Neo4j/Float32Array}
     */
    id_converter( id ) {
        return Neo4j.int( id );
    }
});
